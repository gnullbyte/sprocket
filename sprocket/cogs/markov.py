import os
import time
import random

from discord.ext import commands

from sprocket.util.common import repo_root
from sprocket.util.common import ls


class Markov(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.chain = {}
        self.read_file()
        self.chattiness = 0.05

    def generate(self):
        start = random.choice(list(self.chain.keys()))

        while not start[0][0].isupper():
            start = random.choice(list(self.chain.keys()))

        s = " ".join(start)
        while self.chain[start][0] != "\x02":

            choice = random.choice(self.chain[start])
            start = (start[1], choice)
            s += f" {choice}"

        return s

    def read_file(self):
        directory = repo_root("data", "text")
        match = ls(directory, filter="chain")[0]
        filepath = os.path.join(directory, match)
        if os.path.exists(filepath):
            f = list(filter(lambda x: x != "", open(filepath).read().splitlines()))

            for line in f:
                for pair in self.split_message(line):
                    if (pair[0], pair[1]) in self.chain:
                        self.chain[(pair[0], pair[1])].append(pair[2])
                    else:
                        self.chain[(pair[0], pair[1])] = list()
                        self.chain[(pair[0], pair[1])].append(pair[2])

    def split_message(self, message):
        stop_word = "\x02"
        chain_length = 2
        words = message.split()

        if len(words) > chain_length:
            words.append(stop_word)

            for i in range(len(words) - chain_length):
                yield words[i : i + chain_length + 1]

    def new_sentence(self):
        msg = self.generate()
        while len(msg.split(" ")) < 5:
            msg = self.generate()
        return msg

    async def send_delayed(self, channel, msg):
        await channel.trigger_typing()
        seconds = random.randint(1, 3)
        time.sleep(seconds)
        await channel.send(msg)

    @commands.command(name="mouthy", aliases=["m"], hidden=True)
    @commands.is_owner()
    async def mouthy(self, ctx):
        user = await self.bot.fetch_user(ctx.message.author.id)
        await user.send("```fix\nchattiness: {:03.2f}```".format(self.chattiness))

    @commands.command(name="speakup", aliases=["su", "louder"], hidden=True)
    @commands.is_owner()
    async def speakup(self, ctx):
        if self.chattiness < 0.7:
            self.chattiness += 0.05

    @commands.command(name="shutup", aliases=["q", "quiet"], hidden=True)
    async def shutup(self, ctx):
        if self.chattiness >= 0.1:
            self.chattiness -= 0.05

    @commands.Cog.listener()
    async def on_message(self, message):
        chance = random.random()
        if message.author.id == self.bot.user.id:
            return
        for member in message.mentions:
            if member.id == self.bot.user.id:
                msg = self.new_sentence()
                await message.channel.trigger_typing()
                time.sleep(random.random() + 0.3)
                await message.channel.send(msg)
                return

        if chance < self.chattiness:
            msg = self.new_sentence()
            await self.send_delayed(message.channel, msg)


def setup(bot):
    bot.add_cog(Markov(bot))
