import os
import discord
from discord.ext import commands

from sprocket.util.common import repo_root


class CommandErrorHandler(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        ignored = (commands.CommandNotFound, commands.UserInputError)
        error = getattr(error, "original", error)

        if isinstance(error, ignored):
            return

        if isinstance(error, commands.NotOwner):
            unauthorized = os.path.join(repo_root("data", "img"), "unauthorized.jpg")

            if os.path.exists(unauthorized):
                return await ctx.send(file=discord.File(unauthorized))

            return await ctx.send("You have no power here.")


def setup(bot):
    bot.add_cog(CommandErrorHandler(bot))
