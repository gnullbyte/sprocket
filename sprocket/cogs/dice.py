from dicey.dieparser import DieParser
from discord.ext import commands
from lark import exceptions


class Dice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.dieparser = DieParser()
        self.user_rolls = {}

    def _cycle_user_list(self, key):
        while len(self.user_rolls[key]) > 10:
            self.user_rolls[key] = self.user_rolls[key][1:]

    def _eval_expr(self, expr):
        self.dieparser.parse(expr)
        return self.dieparser.__str__()

    async def _paginate_output(self, ctx, output):
        user = await self.bot.fetch_user(ctx.message.author.id)

        # if output exceeds max message length, paginate
        # it into a private message to the requesting user.
        # otherwise send normally

        paginate = False
        while len(output) > 2000:
            paginate = True
            split = 1900
            while output[split] != "\n":
                split += 1
            msg = "```yaml\n{}```".format(output[:split])
            await user.send(msg)
            output = output[split:]

        if paginate:
            await user.send("```yaml\n{}```".format(output))
        else:
            await ctx.send("```yaml\n{}```".format(output))

    @commands.command(name="roll", aliases=["r"])
    async def roll(self, ctx, *args):
        expr = " ".join(args)

        try:
            result = self._eval_expr(expr)
        except exceptions.UnexpectedCharacters:
            msg = "🎲invalid expression🎲\n"
            msg += "```yaml\nexample: 1d20 + 2d4 -1 + 1d6 + 2```"
            print("lark.exceptions.UnexpectedCharacters caught")
            await ctx.send(msg)
            return

        self.user_rolls.setdefault(ctx.author.id, []).append(expr)
        self._cycle_user_list(ctx.author.id)

        await self._paginate_output(ctx, result)

    @commands.command(name="sr")
    async def sr(self, ctx, *args):
        arg_str = " ".join(args)
        expr = "d6 >=5, =1, =6, <=4"

        if "{" in arg_str:
            start = arg_str.index("{")
            stop = arg_str.index("}") + 1
            expr = "{}".format(arg_str[:start].replace(" ", "")) + expr
            expr += " {}".format(arg_str[start:stop])
            print(expr)

        else:
            expr = "{}".format(arg_str) + expr
            print(expr)

        try:
            result = self._eval_expr(expr)
        except exceptions.Unexpected:
            msg = "🎲invalid expression🎲\n"
            await ctx.send(msg)
            return

        self.user_rolls.setdefault(ctx.author.id, []).append(expr)
        self._cycle_user_list(ctx.author.id)

        await self._paginate_output(ctx, result)

    @commands.command(name="se")
    async def se(self, ctx, *args):
        num_dice = " ".join(args)
        expr = "{}d6 >=5, =1, =6, <=4 !".format(num_dice)

        try:
            result = self._eval_expr(expr)
        except exceptions.Unexpected:
            msg = "🎲invalid expression🎲\n"
            await ctx.send(msg)
            return

        self.user_rolls.setdefault(ctx.author.id, []).append(expr)
        self._cycle_user_list(ctx.author.id)

        await self._paginate_output(ctx, result)

    @commands.group(aliases=["rr"], pass_context=True, invoke_without_command=True)
    async def reroll(self, ctx):
        if ctx.invoked_subcommand is None:
            if ctx.author.id in self.user_rolls:
                expr = self.user_rolls[ctx.author.id][-1]
                result = self._eval_expr(expr)
                await self._paginate_output(ctx, result)

    @reroll.group(aliases=["l"], pass_context=True, invoke_without_command=True)
    async def list(self, ctx):
        roll_history = "```yaml\n"
        if ctx.author.id in self.user_rolls:
            for i, roll in enumerate(self.user_rolls[ctx.author.id]):
                roll_history += "{0} : {1}\n".format(i + 1, roll)
            roll_history += "```"
            await ctx.send(roll_history)
        else:
            await ctx.send("roll history empty")

    @reroll.group(
        aliases=["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"], pass_context=True
    )
    async def selection(self, ctx, *args):
        selection = [int(s) for s in ctx.message.content.split() if s.isdigit()][0]
        result = self._eval_expr(self.user_rolls[ctx.author.id][selection - 1])
        await self._paginate_output(ctx, result)


def setup(bot):
    bot.add_cog(Dice(bot))
