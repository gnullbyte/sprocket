install: poetry.lock
	pre-commit install
	git config --bool flake8.strict true
	poetry install

requirements: poetry.lock
	poetry export -f requirements.txt > requirements.txt

run: docker-compose.yml Dockerfile
	docker-compose build
	docker-compose up -d
	docker-compose logs -f

stop: docker-compose.yml Dockerfile
	docker-compose down

build: setup.py
	python setup.py sdist bdist_wheel

publish: build
	python -m twine upload dist/*

publish-test: build
	python -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

clean:
	rm -rf build/
	rm -rf dist/
	rm -rf *.egg-info/
